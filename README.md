# SISTAVM
The SImple STAck based Virtual Machine

## Installation
```
cargo install --git https://codeberg.org/Meidan_Pelastajamme/sistavm.git
```

## Usage
Help :
```
sistavm help
```

Run file :
```
sistavm run example.sis
```

Execute language tests :
```
sistavm test
```

## Keywords

### Separator
The `;` character can be use to arbitrarily seperate the code, it is simply skipped by the VM.

### Comments
Single line: `// comment`

Multiline : `/* comment */`

### Stack Manipulation

Name            | Usage            | Before         | After
----------------|------------------|----------------|-----------------
Push            | `push x` or `x`  | [\_]           | [\_, x]
Drop            | `drop`           | [\_, x]        | [\_]
Duplication     | `dup`            | [\_, x]        | [\_, x, x]
Size            | `size`           | [x, y]         | [x, y, 2]
Flip            | `flip`           | [x, y, z]      | [z, y, x]
Swap            | `swap`           | [\_, x, y, z]  | [x, z, y]
Over            | `over`           | [\_, x, y]     | [\_, x, y, x]
Yeet            | `yeet`           | [\_]           | []
Concatenation   | `con`            | [str1, str2]   | [str1 + str2]

### Arithmetic

Convetion: Chain arithmetic operators with a space : `+ - * /`, for consistency, beacause: `//` is a comment
Name            | Usage     | Before      | After
----------------|-----------|-------------|-----------------
Addition        | `+`       | [\_, x, y]  | [\_, x + y]
Substraction    | `-`       | [\_, x, y]  | [\_, x - y]
Multiplication  | `*`       | [\_, x, y]  | [\_, x * y]
Division        | `/`       | [\_, x, y]  | [\_, x / y]

### I/O

Name            | Usage     | Before      | After
----------------|-----------|-------------|--------------------------
Print           | `print`   | [\_, x]     | [\_, x] + stdout "x"
Print line      | `printl`  | [\_, x]     | [\_, x] + stdout "x\n"
Dump            | `dump`    | [x, y]      | [x, y] + stdout "[x, y]"
Exit            | `exit`    | [\_, 1]     | exited with 1

### Non-stack operations

Name                  | Usage     
----------------------|------------------------
Variable declaration  | `let <name> <value>`       
Procedure declaration | `def <name> do <body> end`
Procedure call        | `call <name>`       

### Stackable Datatypes
Stackable types are types that can be added to the stack.

Type        | Example
------------|--------------------
Num(i64)    | `push 1` or `1`
Str(String) | `push "a"` or `"a"`

## TODO
### Primary:
- 'not', 'else', 'or', 'and' (basic boolean instructions)
- change `def ... do ... end` to `function ... is ... end`
- maybe `let <name> <value>` to `<value>` is pushed and then `let <name>` that assigns the value to the name
- `if <cond> then <body> endif`
- `if <cond> then <body> else <body> endif`
- over: Add over, over2 and over that bring to the top the 3rd and 4rth elements: [1, 2, 3, _] -> over -> [3, 1, 2, _] && [1, 2, 3, 4, _] -> over2 -> [4, 1, 2, 3, _]
- Const instruction: let but immutable
- Add better tests: Maybe make it a language feature with an assert keyword so that you just need to run a custom test.sis to test. Output capturing to test output features

### Secondary
- while loops
- `openFile`: `[_, filename_str, mode_str]` -> `[_, handle]`
- `closeFile`: `[_, handle]` -> `[]` 
- `readString`: `[_, handle]` -> `[_, handle, file_content_str]`
- `writeString`: `[_, handle, file_content_str]` -> `[_, handle]`
- or `{}`: `[_]` -> `[_, new_list]`
- add to end of list or `::`: `[_, {_}, el]` -> `[_, {_, el}]`
- read list at a certain index `@`: `[_, {4, 3, 2, 1}, 2]` -> `[_, {4, 3, 2, 1}, 3]`
- `split` or maybe `lex`: `[str, delimiter, special_regions]` -> `[list_of_tokens]`; Very doubtful about that instruction;
- maybe juste `split`: `[_, "str of wor ds", " "]` -> `[_, {"str", "of", "wor", "ds"}]`

### Tertiary:
- optimisation: maybe don't actually flip the stack but save the flipped state of the program (make it overall more efficient with stack manipulation, yeah that means using some spooky impure memory shenanigans)

## ROADMAP
1. Have a good enough vm interpreter written in simple rust
2. Write a compiled stripped down sista (not vm) language (different and even simpler (more low-level maybe)) with the sistavm coded in rust
3. Write an interpreter in the barebone sista (the compiled language) to run the full fledged interpreter (sistavm) 
