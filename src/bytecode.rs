use crate::lexer::Token;
use crate::stackable::*;

#[derive(Debug, Clone)]
pub enum Op {
    // Stack manipulation
    Push(Stackable),
    Drop,
    Dup,
    Size,
    Flip,
    Swap,
    Over,
    Yeet,
    Con,
    // Arithmetic
    Add,
    Sub,
    Mul,
    Div,
    // I/O
    Print,
    PrintLine,
    Dump,
    Exit,
    Wait,
    // Control flow
    If(Vec<Op>),
    // Non-stack operations
    Let(String, Stackable),
    Mut(String),
    Def(String, Vec<Op>),
    Call(String),
    // Logical combinators
    Inf,
    Sup,
    Eq,
}

#[derive(Debug)]
pub struct Bytecode {
    pub ops: Vec<Op>,
}

impl Bytecode {
    pub fn new() -> Self {
        Self {
            ops: Default::default(),
        }
    }

    fn inc_if_ends(token: &Token) -> i32 {
        match token {
            Token::Inst(s) if { s == &"if".to_string() }  => 1,
            Token::Inst(s) if { s == &"end".to_string() } => -1,
            _ => 0,
        }
    }
        
    fn tokens_to_ops(tokens: Vec<Token>) -> Vec<Op> {
        let mut ops: Vec<Op> = Default::default();
        let mut i = 0;
        while i < tokens.len() {
            match &tokens[i] {
                t if { is_stackable(t) } => ops.push(Op::Push(t.to_stackable())), // implicit push
                Token::Inst(str) => match str.as_str() {
                    // Stack manipulation
                    "push" => { // explicit push
                        let next_token = tokens
                            .get(i + 1)
                            .unwrap_or_else(|| panic!("Error: expected token after push"));
                        if !is_stackable(next_token) {
                            panic!(
                                "Error: the token after push was not a pushable (got {:?})",
                                next_token
                            );
                        }
                        ops.push(Op::Push(next_token.to_stackable()));
                        i += 1;
                    }
                    "drop" => ops.push(Op::Drop),
                    "dup"  => ops.push(Op::Dup),
                    "size" => ops.push(Op::Size),
                    "flip" => ops.push(Op::Flip),
                    "swap" => ops.push(Op::Swap),
                    "over" => ops.push(Op::Over),
                    "yeet" => ops.push(Op::Yeet),
                    "con"  => ops.push(Op::Con),
                    // Arithmetic
                    "+" => ops.push(Op::Add),
                    "-" => ops.push(Op::Sub),
                    "*" => ops.push(Op::Mul),
                    "/" => ops.push(Op::Div),
                    // I/O
                    "print"  => ops.push(Op::Print),
                    "printl" => ops.push(Op::PrintLine),
                    "dump"   => ops.push(Op::Dump),
                    "exit"   => ops.push(Op::Exit),
                    "wait"   => ops.push(Op::Wait),
                    // Control flow
                    "if" => {
                        i += 1;
                        let mut body: Vec<Token> = Default::default();
                        while tokens[i] != Token::Inst("end".to_string()) {
                            body.push(tokens[i].clone());
                            i += 1;
                        }
                        ops.push(Op::If(Self::tokens_to_ops(body))); 
                    },
                    // Logical operators
                    "<" => ops.push(Op::Inf),
                    ">" => ops.push(Op::Sup),
                    "=" => ops.push(Op::Eq),
                    // Non-stack operations
                    "let" => {
                        i += 1;
                        let next_token = &tokens[i + 1];
                        if is_stackable(&next_token) {
                            match (tokens[i].clone(), next_token.to_stackable()) {
                                (Token::Inst(s), v) => ops.push(Op::Let(s, v)),
                                _ => panic!("Error: let:\n    Expected: Let Ident Stackable \n    Got : Let {:?} {:?}", tokens[i], tokens[i + 1]),
                            }
                            i += 1;
                        } else {
                            panic!("Error: tried to assign a non stackable value to a variable: Let Var: {:?} Val: {:?}", tokens[i], tokens[i + 1]);
                        }
                    },
                    "mut" => {
                        i += 1;
                        let name = match tokens.get(i).unwrap_or_else(|| panic!("Error:\n    expected: 'mut <name>\n got: 'mut' and nothing else")) {
                            Token::Inst(s) => s,
                            e => panic!("Error: mut:\n    expected ident\n    got: {:?}", e),
                        };
                        ops.push(Op::Mut(name.to_string()));
                    }
                    "def" => {
                        i += 1;
                        
                        let name = tokens.get(i).unwrap_or_else(|| panic!("Error:\n    expected: 'def <name> do <body> end\n got: 'def' and nothing else"));
                        let mut end_skip_count = 0;
                        let mut body: Vec<Token> = Default::default();
                        match name {
                            Token::Inst(s) => {
                                i += 1;
                                match &tokens[i] {
                                    Token::Inst(do_key) if { do_key == &"do".to_string() } => {
                                        i += 1;
                                        loop {
                                            if tokens[i] == Token::Inst("end".to_string()) && end_skip_count == 0 {
                                                break;
                                            }
                                            body.push(tokens[i].clone());
                                            end_skip_count += Self::inc_if_ends(&tokens[i]);
                                            i += 1;
                                        }
                                        ops.push(Op::Def(s.to_string(), Self::tokens_to_ops(body))); 
                                        // TODO: check if there is a function declaration in the body and eventually forbid it 
                                    },
                                    do_ => panic!("Error:\n    expected: 'do' after name in procedure declaration\n    got: 'def {} {:?}'", s, do_),
                                }
                            },
                            t => panic!("Error:\n    epxected: valid identifier\n    got: {:?}", t),
                        }
                    },
                    "call" => {
                        let name = tokens.get(i + 1).unwrap_or_else(|| panic!("Error:\n    got: 'call'\n    expected: `call <name>`"));
                        match name {
                            Token::Inst(s) => ops.push(Op::Call(s.to_string())),
                            t => panic!("Error:\n    epxected: valid identifier\n    got: {:?}", t),
                        }
                        i += 1;
                    },
                    ident => ops.push(Op::Push(Stackable::Ident((*ident).to_string()))),
                },
                Token::EOF => {}
                t => panic!("Error: unexpected token (got {:?})", t),
            }
            i += 1;
        }
        return ops;
    }  

    pub fn load_from_tokens(&mut self, tokens: Vec<Token>) {
        self.ops = Self::tokens_to_ops(tokens);
    }

    // pub fn syntactical_analysis(&mut self) {}
}

