use crate::stackable::*;

pub struct Lexer {
    content: Vec<char>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Token {
    Num(i64),
    Str(String),
    Bool(bool),
    Inst(String),
    EOF,
}

impl Token {
    pub fn to_stackable(&self) -> Stackable {
        match self {
            Token::Num(x)  => Stackable::Num(*x),
            Token::Str(s)  => Stackable::Str((*s).to_string()),
            Token::Bool(b) => Stackable::Bool(*b),
            Token::Inst(s) => Stackable::Ident((*s).to_string()),
            _ => panic!("Error: 'to_stackable' unhandled token {:?}", self),
        }
    }
}

fn is_alpha(c: char) -> bool {
    match c {
        'a'..='z' => true,
        _ => false,
    }
}

fn is_inst(c: char) -> bool {
    match c {
        'a'..='z' | '_' | '0'..='9' => true,
        _ => false,
    }
}

fn is_digit(c: char) -> bool {
    match c {
        '0'..='9' => true,
        _ => false,
    }
}

impl Lexer {
    fn advance(&mut self, n: usize) -> &mut Self {
        for _ in 0..n {
            self.content.remove(0);
        }
        self
    }

    fn get_while(&mut self, cond: fn(char) -> bool) -> String {
        if self.content.len() == 0 {
            return "".to_string();
        }
        match self.content[0] {
            c if { cond(c) } => c.to_string() + &self.advance(1).get_while(cond),
            _ => "".to_string(),
        }
    }

    fn get_string(&mut self) -> String {
        if self.content.len() == 0 {
            println!("Error: file terminated before string closed");
            std::process::exit(1);
        }
        match self.content[0] {
            '"' => {
                self.advance(1);
                "".to_string()
            }
            c => c.to_string() + &self.advance(1).get_string(),
        }
    }

    pub fn new(content: String) -> Lexer {
        Self {
            content: content.chars().collect(),
        }
    }

    fn skip_line(&mut self) {
        while !self.content.is_empty() && self.content[0] != '\n' {
            self.advance(1);
        }
        if self.content.is_empty() {
            return;
        }
        self.advance(1);
    }

    fn skip_multiline(&mut self) {
        while !self.content.is_empty() {
            if self.content[0] == '*' && *self.content.get(1).unwrap_or(&'/') == '/' {
                self.advance(1).advance(1);
                return;
            }
            self.advance(1);
        }
        if self.content.is_empty() {
            return;
        }
        self.advance(1);
    }

fn consume_if(&self, expected: Vec<char>) -> bool {
    let mut content_iter = self.content.iter().peekable();

    for &c in &expected {
        if content_iter.peek() != Some(&&c) {
            return false;
        }
        content_iter.next();
    }

    true
}


    fn get_bool(&self, b: bool) -> bool {
        match b {
            true  => self.consume_if("true".to_string().chars().collect()),
            false => self.consume_if("false".to_string().chars().collect()),
        }
    }

    pub fn next(&mut self) -> Token {
        if self.content.is_empty() {
            return Token::EOF;
        }
        match self.content[0] {
            '/' => {
                if self.content.len() > 1 && self.content[1] == '/' {
                    self.skip_line();
                    return self.next();
                }
                if self.content.len() > 1 && self.content[1] == '*' {
                    self.skip_multiline();
                    return self.next();
                }
                self.advance(1);
                return Token::Inst("/".to_string());
            }
            '"' => Token::Str(self.advance(1).get_string()),
            't' if { self.get_bool(true) }  => {
                self.advance(4);
                Token::Bool(true)
            }
            'f' if { self.get_bool(false) } => {
                self.advance(5);
                Token::Bool(false)
            }
            c if { is_alpha(c) } => Token::Inst(self.get_while(is_inst)),
            i if { is_digit(i) } => Token::Num(self.get_while(is_digit).parse::<i64>().unwrap()),
            c if { c == '+' 
                || c == '-' 
                || c == '*' 
                || c == '<' 
                || c == '>' 
                || c == '='
            } => {
                self.advance(1);
                return Token::Inst(c.to_string());
            },
            ';' | '\n' | ' ' => self.advance(1).next(),
            t => panic!("Error: unrecognised char: {:?}", t),
        }
    }

    pub fn consume(&mut self) -> Vec<Token> {
        let mut tokens: Vec<Token> = Default::default();
        loop {
            if self.content.len() == 0 {
                break;
            }
            tokens.push(self.next());
        }
        tokens
    }
}

