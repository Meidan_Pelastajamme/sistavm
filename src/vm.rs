use std::time::Duration;
use std::thread;

use std::collections::HashMap;

use crate::bytecode::*;
use crate::stackable::*;

pub struct Vm {
    pub stack: Vec<Stackable>,
    pub vars: HashMap<String, Stackable>,
    pub procs: HashMap<String, Vec<Op>>, 
}

impl Vm {
    pub fn new() -> Self {
        Self {
            stack: Default::default(),
            vars: HashMap::new(),
            procs: HashMap::new(),
        }
    }

    fn push(&mut self, item: Stackable) {
        self.stack.push(item);
    }

    fn drop(&mut self) {
        self.stack.pop();
    }

    // TODO: implement bool
    fn _is_empty(&self) -> bool {
        self.stack.is_empty()
    }

    fn size(&self) -> usize {
        self.stack.len()
    }

    fn get_var(&self, name: String) -> Stackable {
        self.vars
            .get(&name)
            .unwrap_or_else(|| panic!("Error: {} is not a know variable", name)).clone()
    }

    fn print_action(&self, e: &Stackable) {
        print!("{}", match e {
            Stackable::Ident(s) => self.get_var(s.to_string().clone()),
            _ => e.clone(),
        });
    }

    fn print(&self, new_line: bool) {
        let len = self.stack.len();
        if len == 0 {   
            panic!("Error: nothing to print")
        }
        
        let e = self.stack.get(len - 1).unwrap_or_else(|| unreachable!());
        
        self.print_action(e);

        if new_line {
            print!("\n");
        }
    }

    fn dump(&self) {
        print!("[");
        let len = self.stack.len();
        if len == 0 {
            return;
        }
        let mut i = 0;

        while i < (len - 1) {
            self.print_action(&self.stack[i]);
            print!(", ");
            i += 1;
        }
        self.print_action(&self.stack[i]);

        println!("]");
    }

    fn flip(&mut self) {
        self.stack.reverse();
    }

    fn de_stack_var(&self, e: Option<Stackable>) -> Option<Stackable> {
        match e {
            Some(Stackable::Ident(s)) => Some(self.get_var(s.to_string())),
            el => el,
        }
    }

    fn add(&mut self) -> Option<Stackable> where Stackable: std::ops::Sub<Output = Stackable> + Clone
    {
        let y_item = self.stack.pop().clone();
        let x_item = self.stack.pop().clone();

        let x = self.de_stack_var(x_item);
        let y = self.de_stack_var(y_item);

        match (x, y) {
            (Some(x_val), Some(y_val)) => Some(x_val.clone() + y_val.clone()),
            _ => None,
        }
    }

    fn sub(&mut self) -> Option<Stackable> {
        let y_item = self.stack.pop().clone();
        let x_item = self.stack.pop().clone();
        
        let x = self.de_stack_var(x_item);
        let y = self.de_stack_var(y_item);

        match (x, y) {
            (Some(x_val), Some(y_val)) => Some(x_val - y_val),
            _ => None,
        }
    }

    fn mul(&mut self) {
        let len = self.stack.len();
        if len < 1 {
            println!("Error: not enough arguments on the stack for mul");
            std::process::exit(1);
        }
        match (
            self.stack
                .pop()
                .unwrap_or_else(|| panic!("Shouldn't be reached")),
            self.stack
                .pop()
                .unwrap_or_else(|| panic!("Shouldn't be reached")),
        ) {
            (Stackable::Num(x), Stackable::Num(y)) => self.stack.push(Stackable::Num(x * y)),
            (Stackable::Str(_), Stackable::Str(_)) => {
                panic!("Error: why would you want to mul two strings QQ, you psycho")
            }
            _ => panic!("Error: cannot mul mixed type"),
        }
    }

    fn div(&mut self) {
        let len = self.stack.len();
        if len < 2 {
            println!("Error: not enough arguments on the stack for div");
            std::process::exit(1);
        }
        match (
            self.stack
                .pop()
                .unwrap_or_else(|| panic!("Unreachable")),
            self.stack
                .pop()
                .unwrap_or_else(|| panic!("Unreachable")),
        ) {
            (Stackable::Num(y), Stackable::Num(x)) => self.stack.push(Stackable::Num(x / y)),
            _ => panic!("Error: cannot div anything other than two nums"),
        }
    }

    fn dup(&mut self) {
        if self.stack.len() == 0 {
            panic!("Error: cannot dup 0 elements");
        }
        self.stack.push(self.stack[&self.stack.len() - 1].clone());
    }

    fn swap(&mut self) {
        let last = self.stack.len() - 1;
        (self.stack[last], self.stack[last - 1]) =
            (self.stack[last - 1].clone(), self.stack[last].clone());
    }

    fn over(&mut self) {
        if self.stack.len() < 2 {
            panic!("Error: cannot 'over' with less than 2 values on the stack");
        }
        self.stack.push(self.stack[0].clone());
    }

    fn con(&mut self) {
        let len = self.stack.len();
        if len < 1 {
            println!("Error: not enough arguments on the stack for con");
            std::process::exit(1);
        }
        match (self.stack.pop().unwrap(), self.stack.pop().unwrap()) {
            // cannot replace with 's1 + s2' because it conflicts with the add operator which bans strings
            (s1, s2) => self.stack.push(Stackable::Str(format!("{}{}", s2, s1))),
        }
    }

    fn yeet(&mut self) {
        self.stack = Vec::new();
    }

    fn exit(&mut self) {
        let len = self.stack.len();
        if len == 0 {
            println!("Error: 'exit' no exit code on the stack (exiting with 1)");
            std::process::exit(1);
        }
        let exit_code = *(match &self.stack[len - 1] {
            Stackable::Num(x) => x,
            e => panic!("Error: exit code is not a num (int) {:?}", e),
        }) as i32;

        std::process::exit(exit_code);
    }

    fn wait(&mut self) {
        match self.stack.pop().unwrap_or_else(|| panic!("Error: no elements on the stack to wait")) {
            Stackable::Num(x) => thread::sleep(Duration::from_millis(x.try_into().unwrap())),
            _ => panic!("Error: wait: not a num"),
        }       
    }

    fn let_var(&mut self, name: String, value: Stackable) {
        self.vars.insert(name, value);
    }

    fn def_decl(&mut self, name: String, body: Vec<Op>) {
        self.procs.insert(name, body);
    }

    fn def_call(&mut self, name: String) {
        let body = self.procs.get(&name).unwrap_or_else(|| panic!("Error: could not find the procedure '{}'", name));
        let bytecode = Bytecode {
            ops: body.to_vec(),
        };
        self.execute(bytecode);
    }

    fn if_(&mut self, body: Vec<Op>) {
        let cond = match self.stack.pop().unwrap_or_else(|| panic!("Error: Nothing on the stack for if")) {
            Stackable::Bool(b) => b,
            e => panic!("Error: if expects a bool but got {:?}", e),
        };
        let bytecode = Bytecode {
            ops: body,
        };
        if cond {
            self.execute(bytecode);
        }
    }
    
    fn get_eventual_var(&self, e: Stackable) -> Stackable {
        match e {
            Stackable::Ident(s) => self.get_var(s),
            _ => e,
        }
    }
        
    fn pop_pair(&mut self) -> Option<(Stackable, Stackable)> {
        let e2 = self.stack.pop().unwrap_or_else(|| panic!("Error: expected"));
        let e1 = self.stack.pop().unwrap_or_else(|| panic!("Error: expected"));
        
        let v2 = self.get_eventual_var(e2);
        let v1 = self.get_eventual_var(e1);

        match (v1, v2) {
            (v1, v2) => Some((v1, v2)),
            _ => None,
        }
    }


    fn pop_pair_bool(&mut self) -> Option<(bool, bool)> {
        match self.pop_pair() {
            Some((Stackable::Bool(b2), Stackable::Bool(b1))) => Some((b1, b2)),
            _ => None,
        }
    }

    fn inf(&mut self) {
        match self.pop_pair() {
            Some((Stackable::Num(b1), Stackable::Num(b2))) => self.stack.push(Stackable::Bool(b1 < b2)),
            None => panic!("Error: '<' need two num on top of the stack, got less than 2"),
            Some((v1, v2)) => panic!("Error: '<' need two num got : {:?} & {:?}", v1, v2),
        }
    }

    fn sup(&mut self) {
        match self.pop_pair() {
            Some((Stackable::Num(b1), Stackable::Num(b2))) => self.stack.push(Stackable::Bool(b1 > b2)),
            None => panic!("Error: '>' need two bools on top of the stack, got less than 2"),
            Some((v1, v2)) => panic!("Error: '>' need two num got : {:?} & {:?}", v1, v2),
        }
    }
    
    fn eq(&mut self) {
        match self.pop_pair() {
            Some((Stackable::Num(b1), Stackable::Num(b2))) => self.stack.push(Stackable::Bool(b1 == b2)),
            None => panic!("Error: '=' need two bools on top of the stack, got less than 2"),
            Some((v1, v2)) => panic!("Error: '=' need two num got : {:?} & {:?}", v1, v2),
        }
    }

    fn mut_(&mut self, name: String) {
        let current = self.get_var(name.clone());
        let new = self.stack.pop().unwrap_or_else(|| panic!("Error: mut:\n    Nothing on the stack to mutate"));
        
        if std::mem::discriminant(&current) != std::mem::discriminant(&new) {
            panic!("Error: cannot mutate to a different type\n    {:?} -> {:?}", current, new);
        }
        *self.vars.get_mut(&name)
            .unwrap_or_else(|| panic!("Error:\n    got: mut {}\n    unknow var {}", name, name))
            = new;
    }

    pub fn execute(&mut self, bytecode: Bytecode) {
        bytecode.ops.into_iter().for_each(|op| match op {
            Op::Push(pushable) => self.push(pushable),
            Op::Print => self.print(false),
            Op::PrintLine => self.print(true),
            Op::Drop => self.drop(),
            Op::Dump => self.dump(),
            Op::Size => self.push(Stackable::Num(self.size() as i64)),
            Op::Flip => self.flip(),
            Op::Add => {
                if let Some(result) = self.add() {
                    self.push(result);
                } else {
                    panic!("Error: not enough argument to add");
                }
            }
            Op::Sub => {
                if let Some(result) = self.sub() {
                    self.push(result);
                } else {
                    panic!("Error: not enough argument to add");
                }
            }
            Op::Mul => self.mul(),
            Op::Div => self.div(),
            Op::Dup => self.dup(),
            Op::Swap => self.swap(),
            Op::Over => self.over(),
            Op::Con => self.con(),
            Op::Yeet => self.yeet(),
            Op::Exit => self.exit(),
            Op::Wait => self.wait(),
            // Control flow
            Op::If(body) => self.if_(body),
            // Non stack manipulations
            Op::Let(name, val) => self.let_var(name, val),
            Op::Mut(name) => self.mut_(name),
            Op::Def(name, body) => self.def_decl(name, body),
            Op::Call(name) => self.def_call(name),
            // Logical operators
            Op::Inf => self.inf(),
            Op::Sup => self.sup(),
            Op::Eq  => self.eq(),
        });
    }
}

