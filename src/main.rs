use std::env;

pub mod lexer;
pub mod bytecode;
pub mod vm;
pub mod stackable;

mod cli;
use cli::{run_cli};


fn main() {
    let args: Vec<String> = env::args().collect();
    run_cli(args);
}
