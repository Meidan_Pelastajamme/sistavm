use std::fs::File;
use std::io::prelude::*;

use crate::lexer::*;
use crate::bytecode::*;
use crate::vm::*;

fn run(input: String) {
    let tokens = Lexer::new(input).consume();
    let mut bytecode = Bytecode::new();
    bytecode.load_from_tokens(tokens);
    // bytecode.syntactical_analysis(); // TODO
    let mut vm: Vm = Vm::new();
    vm.execute(bytecode);
}

fn test() {
    let tests: Vec<(&str, &str)> = vec![
        ("push 10", ""),
        ("push 1; push 2; push 3; +; dump", "[1, 5]"),
        ("push 1; push 2; push 3; -; dump", "[1, 1]"),
        ("push 10; print", "10"),
        ("push 7; dup; print", "7"),
        ("push 2; push 7; dump", "[2, 7]"),
        ("push 1; push 2; size; print", "2"),
        ("push 1; push 2; push 3; flip; dump", "[3, 2, 1]"),
        ("push 1; push 2; push 3; swap; dump", "[1, 3, 2]"),
        ("push 2; push 3; over; dump", "[2, 3, 2]"),
        ("push 1; push 2; push 4; +; dump", "[1, 6]"),
        ("push 2; push 3; *; dump", "[6]"),
        ("push 2; push 6; /; dump", "[3]"),
    ];

    for test in &tests {
        match test {
            (input, expect) => {
                run(input.to_string());
                println!("{:?}\n", expect.to_string());
            }
        }
    }
}

const CLI_USAGE: &str = "Usage: sistavm <option/s>
  test: `sistavm test example.sis`      only runs the tests of a file
  run : `sistavm run example.sis`       run the file's code
  help: `sistavm help`                  print this help message
";

pub fn run_cli(args: Vec<String>) {
    let mut i = 1;
    if args.len() == 1 {
        println!("No argument provided");
        println!("Try running the program with the argument 'help' to have a manual");
        std::process::exit(1);
    }
    while i < args.len() {
        match args[i].as_str() {
            "test" => test(),
            "run" => {
                i += 1;
                let path = args.get(i).unwrap_or_else(|| {
                    println!("Error: no path provided to run");
                    std::process::exit(1);
                });
                let mut file = File::open(path).expect(
                    format!("Error: unable to open the file to run (path: {})", path).as_str(),
                );
                let mut contents = String::new();
                file.read_to_string(&mut contents).expect(
                    format!("Error: unable to read the file to run (path: {})", path).as_str(),
                );

                run(contents);
            }
            "help" => {
                println!("{}", CLI_USAGE);
                std::process::exit(1);
            }
            _ => {
                println!("Error: unknown argument : {}", args[i]);
                std::process::exit(1);
            }
        }
        i += 1;
    }
}

