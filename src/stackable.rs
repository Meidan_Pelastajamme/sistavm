use std::fmt;
use std::ops::Add;
use std::ops::Sub;

use crate::lexer::Token;

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub enum Stackable {
    Num(i64),
    Str(String),
    Bool(bool),
    Ident(String),
}

impl From<i64> for Stackable {
    fn from(value: i64) -> Self {
        Stackable::Num(value)
    }
}

impl Add for Stackable {
    type Output = Stackable;

    fn add(self, other: Stackable) -> Stackable {
        match (self, other) {
            (Stackable::Num(x), Stackable::Num(y)) => Stackable::Num(x + y),
            (Stackable::Str(_), Stackable::Str(_)) => {
                panic!("Error: cannot use add for two strings (it's not javascript mate)")
            }
            (v1, v2) => panic!("Error: cannot '+' two non 'num' values (got: {:?} {:?}", v1, v2),
        }
    }
}

impl Sub for Stackable {
    type Output = Stackable; 

    fn sub(self, other: Stackable) -> Stackable {
        match (self, other) {
            (Stackable::Num(x), Stackable::Num(y)) => Stackable::Num(x - y),
            (Stackable::Str(_), Stackable::Str(_)) => {
                panic!("Error: cannot use sub for two strings (it's not js)")
            }
            (v1, v2) => panic!("Error: cannot '-' two non 'num' values (got: {:?} {:?}", v1, v2),
        }
    }
}

fn bool_to_str(b: &bool) -> &str {
    match b {
        &true => "true",
        &false => "false",
    }
}

impl fmt::Display for Stackable {

    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Stackable::Num(x)   => write!(f, "{}", x),
            Stackable::Str(s)   => write!(f, "{}", s),
            Stackable::Bool(b)  => write!(f, "{}", bool_to_str(&b)),
            Stackable::Ident(_) => unreachable!("The ident is transformed before"),
        }
    }
}

pub fn is_stackable(token: &Token) -> bool {
    match token {
        Token::Num(_) | Token::Str(_) | Token::Bool(_) => true,
        _ => false,
    }
}

